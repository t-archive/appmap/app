import {Inject, Injectable} from '@angular/core';
import {
    BackgroundGeolocation
} from '@ionic-native/background-geolocation/ngx';
import {GeoSearchControl, OpenStreetMapProvider} from 'leaflet-geosearch';
import {MyMarker} from '../class/MyMarker';
import {SettingsService} from './settings.service';
import * as L from 'leaflet';
import {Canvas, LatLngExpression, LeafletEvent, LeafletMouseEvent} from 'leaflet';
import {MarkerService} from './marker.service';
import {MarkerIcons} from '../keys/MarkerIcons';
import {LocationService} from './location.service';
import {PolylineService} from './polyline.service';
import {Storage} from '@ionic/storage';
import {MAP_CENTER} from '../keys/StorageKeys';
import {Router} from '@angular/router';
import {routes, TabsPageRoutingModule} from '../tabs/tabs-routing.module';


@Injectable({
    providedIn: 'root'
})
export class MapService {
    private _map: L.Map;

    private _mapGroup: L.MarkerClusterGroup;
    private _renderer: L.Canvas = null;
    private _currentMapRouter: L.Routing.Control = null;

    constructor(@Inject(SettingsService) private _settingsService: SettingsService,
                @Inject(LocationService) private _locationService: LocationService,
                @Inject(PolylineService) private _polylineService: PolylineService,
                private storage: Storage,
                private backgroundGeolocation: BackgroundGeolocation,
                @Inject(Router) private router: Router,
                @Inject(MarkerService) private _markerService: MarkerService) {
        this.markerService.mapService = this;
        this.polylineService.mapService = this;

        this.locationService.mapService = this;
    }


    public async start(): Promise<void> {
        return this.settingsService.getValueFromStorage().then(() => {
            this.initMap();
            this.clickEvent();
            this.searchFunction();
            this.goToService();
            this.setMarkerService();
            return this.markerService.loadMarker().then(() => {
                this.markerService.setLiveMarker();
                return this.locationService.start();
            });
        });
    }


    private initMap(): void {
        if (!this.map) {
            let center: L.LatLng = new L.LatLng(51.67441239852524, 7.826029622347606);
            this.storage.get(MAP_CENTER).then((x: any) => {
                if (x) {
                    center = x;
                }
            });
            this.map = L.map('map', {
                center: center,
                zoom: 10,
                layers: [L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 19,
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                })]
            }).invalidateSize();
            this.renderer = L.canvas();
            this.renderer.addTo(this.map);
            setInterval(() => {
                this.storage.set(MAP_CENTER, this.map.getCenter());
            }, 1000);
        }
        setTimeout(() => {
            this.resizeMap();
        }, 1000);

        this.currentMapRouter = L.Routing.control({
            waypoints: [],
            show: false,
            waypointMode: 'connect',
            addWaypoints: false,
            autoRoute: true,
            showAlternatives: false,
            fitSelectedRoutes: false,
            pointMarkerStyle: {
                fill: false,
                fillOpacity: 0,
                opacity: 0
            },
            // router: new L.Routing.OSRMv1({
            //     serviceUrl: "https://maps.googleapis.com/maps/api/directions"
            // }),
            // @ts-ignore
            createMarker: () => null
        });

        this.mapGroup = L.markerClusterGroup({
            chunkedLoading: true,
            spiderfyOnMaxZoom: false,
        });
        this.map.addControl(this.currentMapRouter);
        this.map.addLayer(this.mapGroup);
        this.map.addLayer(this.polylineService.polylineFeatureGroup);
        this.settingsService.showPolylineSubject.subscribe((active) => {
            if (active === true) {
                this.map.addLayer(this.polylineService.polylineFeatureGroup);
            } else {
                this.map.removeLayer(this.polylineService.polylineFeatureGroup);
            }
        });
        this.settingsService.showMarkerSubject.subscribe((active) => {
            setTimeout(() => this.markerService.blockSetMarker = !active, 100);
            setTimeout(() => this.markerService.blockSetMarkerTemp = !active, 100);
            if (active === true) {
                this.map.addLayer(this.mapGroup);
            } else {
                this.map.removeLayer(this.mapGroup);
            }
        });
    }


    private clickEvent(): void {
        this.map.on('click', ((e: LeafletMouseEvent) => {
            const item: L.Marker = L.marker([e.latlng.lat, e.latlng.lng], MarkerIcons.markerIcon());
            const myMarker: MyMarker = new MyMarker();
            myMarker.marker = item;
            myMarker.lat = e.latlng.lat;
            myMarker.lng = e.latlng.lng;
            myMarker.description = '';
            this.markerService.addMarker(myMarker);
        }));
        this.map.on('contextmenu', ((e: LeafletMouseEvent) => {
        }));
    }


    private searchFunction(): void {
        const searchControl: GeoSearchControl = new GeoSearchControl({
            provider: new OpenStreetMapProvider(),
            style: 'bar',
            showMarker: true,
            showPopup: false,
            marker: {
                icon: MarkerIcons.markerIcon().icon,
                draggable: false
            },
            popupFormat: ({query, result}) => result.label,
            maxMarkers: 1,
            retainZoomLevel: false,
            animateZoom: true,
            autoClose: true,
            searchLabel: 'Suche...',
            keepResult: false,
        });
        this.map.on('geosearch/showlocation', (e: LeafletEvent | any) => {
            console.log(e);
            console.log(e.marker);
            const item: L.Marker = L.marker([e.location.y, e.location.x], MarkerIcons.markerIcon());
            const myMarker: MyMarker = new MyMarker();
            myMarker.marker = item;
            myMarker.lat = e.location.y;
            myMarker.lng = e.location.x;
            myMarker.description = e.label;
            myMarker.description = e.location.label;
            this.markerService.addMarker(myMarker);
        });
        this.map.addControl(searchControl);
    }


    private goToService(): void {
        this.markerService.goToMarker.subscribe(item => {
            let zoom: number = 16;
            if (item.marker.getIcon() === MarkerIcons.trackerIcon().icon) {
                zoom = this.map.getZoom();
            }
            this.map.setView({
                lat: item.lat,
                lng: item.lng
            }, zoom, {animate: true});
            setTimeout(() => {
                this.resizeMap();
                if (this.markerService.enabledPopup) {
                    item.marker.openPopup();
                }
            }, 500);
        });
    }

    private setMarkerService(): void {
        this.markerService.setMarker.subscribe((item) => {
            item.marker.bindPopup(item.description ? item.description : item.lat + '/' + item.lng);
            item.marker.bindTooltip(item.description ? item.description : item.lat + '/' + item.lng);
            item.marker.addTo(this.mapGroup);
            // this.mapGroup.addLayer(item.marker);
            if (this.markerService.enabledPopup) {
                item.marker.openPopup();
            }

            this.markerService.findLocation(item);
            this.markerService.reload.subscribe((temp) => {

                item.marker.setPopupContent(item.description ? item.description : item.lat + '/' + item.lng);
                item.marker.setTooltipContent(item.description ? item.description : item.lat + '/' + item.lng);
            });
            item.marker.on('click', () => {
                item.marker.setPopupContent(item.description ? item.description : item.lat + '/' + item.lng);
                item.marker.closeTooltip();
                // item.marker.setTooltipContent(item.description ? item.description : item.lat + '/' + item.lng);
            });
            item.marker.on('mouseover', () => {
                // item.marker.setPopupContent(item.description ? item.description : item.lat + '/' + item.lng);
                item.marker.setTooltipContent(item.description ? item.description : item.lat + '/' + item.lng);
            });
        });


        this.markerService.setRoutingMarker.subscribe(item => {
            const routerItemArray: &L.LatLng[] & L.Routing.Waypoint[] = [];
            this.markerService.list.forEach((myMarker: &MyMarker) => {
                if (myMarker.routing) {
                    myMarker.generateWaypoint();
                    routerItemArray.push(myMarker.waypoint);
                }
            });
            this.currentMapRouter.setWaypoints(routerItemArray);
        });

    }


    public resizeMap(): void {
        if (this.map) {
            this.map.invalidateSize(true);
        }
    }


    get locationService(): LocationService {
        return this._locationService;
    }


    get markerService(): MarkerService {
        return this._markerService;
    }


    get settingsService(): SettingsService {
        return this._settingsService;
    }

    get currentMapRouter(): L.Routing.Control {
        return this._currentMapRouter;
    }

    set currentMapRouter(value: L.Routing.Control) {
        this._currentMapRouter = value;
    }

    get mapGroup(): L.MarkerClusterGroup {
        return this._mapGroup;
    }

    set mapGroup(value: L.MarkerClusterGroup) {
        this._mapGroup = value;
    }

    get map(): L.Map {
        return this._map;
    }

    set map(value: L.Map) {
        this._map = value;
    }

    get polylineService(): PolylineService {
        return this._polylineService;
    }

    get renderer(): Canvas {
        return this._renderer;
    }

    set renderer(value: Canvas) {
        this._renderer = value;
    }
}
