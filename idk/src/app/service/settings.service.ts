import {Injectable} from '@angular/core';
import {
    BACKGROUND_UPDATE,
    LIVE_TRACKING,
    ROUTE_RECORD_POLYLINE, ROUTE_SHOW_MARKER,
    ROUTE_SHOW_POLYLINE,
    SAVE_BATTERY_ON_BACKGROUND,
    TRACKING
} from '../keys/StorageKeys';
import {Storage} from '@ionic/storage';
import {BehaviorSubject, observable, Observable, Subject, Subscriber} from 'rxjs';
import * as L from 'leaflet';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {


    private _tracking: boolean = false;
    private _trackingSubject: Subject<boolean> = new Subject<boolean>();
    private _liveTracking: boolean = false;
    private _liveTrackingSubject: Subject<boolean> = new Subject<boolean>();
    private _saveBatteryOnBackground: boolean = false;
    private _saveBatteryOnBackgroundSubject: Subject<boolean> = new Subject<boolean>();
    private _backgroundUpdate: boolean = true;
    private _backgroundUpdateSubject: Subject<boolean> = new Subject<boolean>();
    private _loadSettingsFromStorage: boolean = false;
    private _showPolyline: boolean = true;
    private _showPolylineSubject: Subject<boolean> = new Subject<boolean>();
    private _showMarker: boolean = true;
    private _showMarkerSubject: Subject<boolean> = new Subject<boolean>();
    private _recordedPolyline: boolean = false;
    private _recordedPolylineSubject: Subject<boolean> = new Subject<boolean>();
    private _mapCenter: L.LatLngExpression = null;
    private _mapCenterSubject: Subject<L.LatLngExpression> = new Subject<L.LatLngExpression>();
    private _updateSettings: Subject<boolean> = new Subject<boolean>();

    constructor(private storage: Storage) {
        this._trackingSubject.subscribe((callback) => {this.updateSettings.next(true); } );
        this._liveTrackingSubject.subscribe((callback) => {this.updateSettings.next(true); } );
        this._saveBatteryOnBackgroundSubject.subscribe((callback) => {this.updateSettings.next(true); } );
        this._backgroundUpdateSubject.subscribe((callback) => {this.updateSettings.next(true); } );
        this._showPolylineSubject.subscribe((callback) => {this.updateSettings.next(true); } );
        this._showMarkerSubject.subscribe((callback) => {this.updateSettings.next(true); } );
        this._recordedPolylineSubject.subscribe((callback) => {this.updateSettings.next(true); } );
        // this.getValueFromStorage().then();
    }

    public async getValueFromStorage(): Promise<void> {
        await Promise.all([
            this.storage.get(LIVE_TRACKING).then(x => {
                if (x !== undefined) {
                    this.liveTracking = x;
                }
            }),
            this.storage.get(TRACKING).then(x => {
                if (x !== undefined) {
                    this.tracking = x;
                }
            }),
            this.storage.get(SAVE_BATTERY_ON_BACKGROUND).then(x => {
                if (x !== undefined) {
                    this.saveBatteryOnBackground = x;
                }
            }),
            this.storage.get(BACKGROUND_UPDATE).then(x => {
                if (x !== undefined) {
                    this.backgroundUpdate = x;
                }
            }),
            this.storage.get(ROUTE_SHOW_POLYLINE).then((x) => {
                if (x !== undefined) {
                    this.showPolyline = x;
                }
            }),
            this.storage.get(ROUTE_RECORD_POLYLINE).then((x) => {
                if (x !== undefined) {
                    this.recordedPolyline = x;
                }
            }),
            this.storage.get(ROUTE_SHOW_MARKER).then((x) => {
                if (x !== undefined) {
                    this.showMarker = x;
                }
            })
        ]).then(value => {
            this._loadSettingsFromStorage = true;
        });

    }

    get liveTracking(): boolean {
        return this._liveTracking;
    }

    set liveTracking(value: boolean) {
        this.storage.set(LIVE_TRACKING, value);
        this.liveTrackingSubject.next(value);
        this._liveTracking = value;
    }

    get tracking(): boolean {
        return this._tracking;
    }

    set tracking(value: boolean) {
        this.storage.set(TRACKING, value);
        this.trackingSubject.next(value);
        if (!value) {
            this.liveTracking = value;
        }
        this._tracking = value;
    }

    get liveTrackingSubject(): Subject<boolean> {
        return this._liveTrackingSubject;
    }

    set liveTrackingSubject(value: Subject<boolean>) {
        this._liveTrackingSubject = value;
    }

    get trackingSubject(): Subject<boolean> {
        return this._trackingSubject;
    }

    set trackingSubject(value: Subject<boolean>) {
        this._trackingSubject = value;
    }

    get saveBatteryOnBackgroundSubject(): Subject<boolean> {
        return this._saveBatteryOnBackgroundSubject;
    }

    set saveBatteryOnBackgroundSubject(value: Subject<boolean>) {
        this._saveBatteryOnBackgroundSubject = value;
    }

    get saveBatteryOnBackground(): boolean {
        return this._saveBatteryOnBackground;
    }

    set saveBatteryOnBackground(value: boolean) {
        this.storage.set(SAVE_BATTERY_ON_BACKGROUND, value);
        this.saveBatteryOnBackgroundSubject.next(value);
        this._saveBatteryOnBackground = value;

    }

    get backgroundUpdate(): boolean {
        return this._backgroundUpdate;
    }

    set backgroundUpdate(value: boolean) {
        this.storage.set(BACKGROUND_UPDATE, value);
        this.backgroundUpdateSubject.next(value);
        this._backgroundUpdate = value;
    }

    get backgroundUpdateSubject(): Subject<boolean> {
        return this._backgroundUpdateSubject;
    }

    set backgroundUpdateSubject(value: Subject<boolean>) {
        this._backgroundUpdateSubject = value;
    }

    get loadSettingsFromStorage(): boolean {
        return this._loadSettingsFromStorage;
    }

    set loadSettingsFromStorage(value: boolean) {
        this._loadSettingsFromStorage = value;
    }

    get showPolyline(): boolean {
        return this._showPolyline;
    }

    set showPolyline(value: boolean) {
        this._showPolyline = value;
        this.storage.set(ROUTE_SHOW_POLYLINE, value);
        this.showPolylineSubject.next(value);
    }

    get recordedPolyline(): boolean {
        return this._recordedPolyline;
    }

    set recordedPolyline(value: boolean) {
        this.recordedPolylineSubject.next(value);
        this.storage.set(ROUTE_RECORD_POLYLINE, value);
        this._recordedPolyline = value;
    }

    get recordedPolylineSubject(): Subject<boolean> {
        return this._recordedPolylineSubject;
    }

    set recordedPolylineSubject(value: Subject<boolean>) {
        this._recordedPolylineSubject = value;
    }


    get showPolylineSubject(): Subject<boolean> {
        return this._showPolylineSubject;
    }

    set showPolylineSubject(value: Subject<boolean>) {
        this._showPolylineSubject = value;
    }

    get showMarkerSubject(): Subject<boolean> {
        return this._showMarkerSubject;
    }

    set showMarkerSubject(value: Subject<boolean>) {
        this._showMarkerSubject = value;
    }

    set showMarker(value: boolean) {
        this._showMarker = value;
        this.showMarkerSubject.next(value);
        this.storage.set(ROUTE_SHOW_MARKER, value);
    }

    get showMarker(): boolean {
        return this._showMarker;
    }
    get updateSettings(): Subject<boolean> {
        return this._updateSettings;
    }

    set updateSettings(value: Subject<boolean>) {
        this._updateSettings = value;
    }
}
