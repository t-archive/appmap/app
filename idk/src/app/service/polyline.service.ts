import {Inject, Injectable} from '@angular/core';
import * as L from 'leaflet';
import {MapService} from './map.service';
import {LocationService} from './location.service';
import {LatLngExpression, Polyline} from 'leaflet';
import {Storage} from '@ionic/storage';
import {LOCATION_LIST, ROUTE_SHOW_POLYLINE} from '../keys/StorageKeys';
import {BackgroundGeolocationResponse} from '@ionic-native/background-geolocation';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PolylineService {


    private _mapService: MapService = null;
    private _polylineList: L.Polyline[] = [];

    private _polylineFeatureGroup: L.FeatureGroup = new L.FeatureGroup<any>();

    constructor(@Inject(LocationService) private locationService: LocationService, private storage: Storage) {
        this.locationService.polylineService = this;
    }


    public async loadLinesFromStorage(): Promise<void> {
        return this.storage.get(LOCATION_LIST).then((locationArray: BackgroundGeolocationResponse[]) => {
            // this.createLine(null, {color: 'red'}, locationArray);
            if (locationArray) {
                locationArray.sort((x, y) => {
                    return x.time - y.time;
                });
                // return this.createLine(null, {color: 'black'}, locationArray);  // TODO: RED
                locationArray.forEach((item) => {
                    this.createLine(item, {color: 'black'});  // TODO: RED
                });
            }
        });
    }


    public async syncLocationStorage(): Promise<void> {
        return this.storage.set(LOCATION_LIST, this.locationService.locationList);
    }

    /**
     * @param item
     * @param option
     * @param anotherPolyline
     * @param accuracy (0 - 7)
     * @param rec
     */
    public createLine(item: BackgroundGeolocationResponse, option: L.PolylineOptions = {color: 'black'}, anotherPolyline: BackgroundGeolocationResponse[] = null, accuracy: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 = 0, rec: boolean = true): void {
        if (this.mapService.renderer && this.locationService.locationList && (anotherPolyline || (this.locationService.locationList.length > 1 && this.locationService.locationList[this.locationService.locationList.length - 1]))) {
            option.renderer = this.mapService.renderer;
            let anotherPolylineArray: L.LatLngExpression[] = [];
            if (item) {
                if (parseFloat(this.locationService.locationList[this.locationService.locationList.length - 1].latitude.toFixed(accuracy)) === parseFloat(item.latitude.toFixed(accuracy)) &&
                    parseFloat(this.locationService.locationList[this.locationService.locationList.length - 1].longitude.toFixed(accuracy)) === parseFloat(item.longitude.toFixed(accuracy))) {
                    anotherPolylineArray = [
                        [this.locationService.locationList[this.locationService.locationList.length - 1].latitude, this.locationService.locationList[this.locationService.locationList.length - 1].longitude],
                        [item.latitude, item.longitude],
                    ];
                }
            } else if (anotherPolyline) {
                anotherPolylineArray = [];
                anotherPolyline.forEach((x) => {
                    if (x.latitude && x.longitude) {
                        anotherPolylineArray.push([x.latitude, x.longitude]);
                    }
                });
            }
            const polyline: L.Polyline = new L.Polyline(anotherPolylineArray, option);
            if (!rec) {
                polyline.setStyle({
                    opacity: 0
                });
            }
            this.polylineFeatureGroup.addLayer(polyline);
            this._polylineList.push(polyline);

        }
        if (item) {
            this.locationService.locationList.push(item);
        } else if (anotherPolyline) {
            anotherPolyline.forEach((xItem) => {
                this.locationService.locationList.push(xItem);
            });
        }
    }

    public async deleteLine(): Promise<void> {
        // await this.locationService.backgroundGeolocation.stop();
        this.polylineFeatureGroup.removeFrom(this.mapService.map);
        this.polylineFeatureGroup.clearLayers();
        this.polylineList = [];
        this.locationService.locationList = [];
        await this.storage.set(LOCATION_LIST, this.locationService.locationList);
        await this.storage.remove(LOCATION_LIST);
        await this.locationService.backgroundGeolocation.deleteAllLocations();
        this.polylineFeatureGroup.addTo(this.mapService.map);
    }


    get mapService(): MapService {
        return this._mapService;
    }

    set mapService(value: MapService) {
        this._mapService = value;
    }


    get polylineList(): Polyline[] {
        return this._polylineList;
    }

    set polylineList(value: Polyline[]) {
        this._polylineList = value;
    }

    get polylineFeatureGroup(): L.FeatureGroup {
        return this._polylineFeatureGroup;
    }

    set polylineFeatureGroup(value: L.FeatureGroup) {
        this._polylineFeatureGroup = value;
    }
}
