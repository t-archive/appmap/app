import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';
import {MapComponent} from '../component/map/map.component';
import {ListComponent} from '../component/list/list.component';
import {SettingsComponent} from '../component/settings/settings.component';

export const routes: Routes = [
    {
        path: '',
        component: TabsPage,
        children: [
            {
                path: 'map',
                component: MapComponent
            },
            {
                path: 'list',
                component: ListComponent
            },
            {
                path: 'settings',
                component: SettingsComponent
            },
            {
                path: '',
                redirectTo: '/map',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/map',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class TabsPageRoutingModule {
}
