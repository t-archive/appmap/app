import {checkIfNumberExists} from '../helper/FactoryHelper';
import {
    BACKGROUND_GEOLOCATION_ITEM_ACCURACY, BACKGROUND_GEOLOCATION_ITEM_ALTITUDE, BACKGROUND_GEOLOCATION_ITEM_BEARING,
    BACKGROUND_GEOLOCATION_ITEM_ID,
    BACKGROUND_GEOLOCATION_ITEM_LATITUDE, BACKGROUND_GEOLOCATION_ITEM_LONGITUDE, BACKGROUND_GEOLOCATION_ITEM_SPEED,
    BACKGROUND_GEOLOCATION_ITEM_TIME
} from '../keys/BackgroundGeolocationItemKeys';
import {BackgroundGeolocationResponse} from '@ionic-native/background-geolocation';

export class BackgroundGeolocationItem {

    private _id: number;
    private _time: number;
    private _latitude: number;
    private _longitude: number;
    private _accuracy: number;
    private _speed: number;
    private _altitude: number;
    private _bearing: number;


    public toJSON(): object {
        const item: object = {};

        item[BACKGROUND_GEOLOCATION_ITEM_ID] = this.id;
        item[BACKGROUND_GEOLOCATION_ITEM_TIME] = this.time;
        item[BACKGROUND_GEOLOCATION_ITEM_LATITUDE] = this.latitude;
        item[BACKGROUND_GEOLOCATION_ITEM_LONGITUDE] = this.longitude;
        item[BACKGROUND_GEOLOCATION_ITEM_ACCURACY] = this.accuracy;
        item[BACKGROUND_GEOLOCATION_ITEM_SPEED] = this.speed;
        item[BACKGROUND_GEOLOCATION_ITEM_ALTITUDE] = this.altitude;
        item[BACKGROUND_GEOLOCATION_ITEM_BEARING] = this.bearing;
        return item;
    }


    public static factory(raw: any): BackgroundGeolocationItem {
        const item: BackgroundGeolocationItem = new BackgroundGeolocationItem();
        if (raw) {

            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_ID) && raw[BACKGROUND_GEOLOCATION_ITEM_ID]) {
                item.id = raw[BACKGROUND_GEOLOCATION_ITEM_ID];
            }
            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_TIME) && raw[BACKGROUND_GEOLOCATION_ITEM_TIME]) {
                item.time = raw[BACKGROUND_GEOLOCATION_ITEM_TIME];
            }
            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_LATITUDE) && raw[BACKGROUND_GEOLOCATION_ITEM_LATITUDE]) {
                item.latitude = raw[BACKGROUND_GEOLOCATION_ITEM_LATITUDE];
            }
            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_LONGITUDE) && raw[BACKGROUND_GEOLOCATION_ITEM_LONGITUDE]) {
                item.longitude = raw[BACKGROUND_GEOLOCATION_ITEM_LONGITUDE];
            }
            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_ACCURACY) && raw[BACKGROUND_GEOLOCATION_ITEM_ACCURACY]) {
                item.accuracy = raw[BACKGROUND_GEOLOCATION_ITEM_ACCURACY];
            }
            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_SPEED) && raw[BACKGROUND_GEOLOCATION_ITEM_SPEED]) {
                item.speed = raw[BACKGROUND_GEOLOCATION_ITEM_SPEED];
            }
            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_ALTITUDE) && raw[BACKGROUND_GEOLOCATION_ITEM_ALTITUDE]) {
                item.altitude = raw[BACKGROUND_GEOLOCATION_ITEM_ALTITUDE];
            }
            if (checkIfNumberExists(raw, BACKGROUND_GEOLOCATION_ITEM_BEARING) && raw[BACKGROUND_GEOLOCATION_ITEM_BEARING]) {
                item.bearing = raw[BACKGROUND_GEOLOCATION_ITEM_BEARING];
            }
        }
        return item;
    }

    public static factoryArray(raw: any): BackgroundGeolocationItem[] {
        const array: BackgroundGeolocationItem[] = [];
        for (const item of raw) {
            array.push(this.factory(item));
        }
        return array;
    }

    public static transform(value: BackgroundGeolocationResponse & any): BackgroundGeolocationItem {
        const item: BackgroundGeolocationItem = new BackgroundGeolocationItem();
        if (value._id) {
            item.id = value._id;
        }
        if (value._time) {
            item.time = value._time;
        }
        if (value._latitude) {
            item.latitude = value._latitude;
        }
        if (value._longitude) {
            item.longitude = value._longitude;
        }
        if (value._accuracy) {
            item.accuracy = value._accuracy;
        }
        if (value._speed) {
            item.speed = value._speed;
        }
        if (value._altitude) {
            item.altitude = value._altitude;
        }
        if (value._bearing) {
            item.bearing = value._bearing;
        }
        if (value.id) {
            item.id = value.id;
        }
        if (value.time) {
            item.time = value.time;
        }
        if (value.latitude) {
            item.latitude = value.latitude;
        }
        if (value.longitude) {
            item.longitude = value.longitude;
        }
        if (value.accuracy) {
            item.accuracy = value.accuracy;
        }
        if (value.speed) {
            item.speed = value.speed;
        }
        if (value.altitude) {
            item.altitude = value.altitude;
        }
        if (value.bearing) {
            item.bearing = value.bearing;
        }
        return item;
    }

    get bearing(): number {
        return this._bearing;
    }

    set bearing(value: number) {
        this._bearing = value;
    }

    get altitude(): number {
        return this._altitude;
    }

    set altitude(value: number) {
        this._altitude = value;
    }

    get speed(): number {
        return this._speed;
    }

    set speed(value: number) {
        this._speed = value;
    }

    get accuracy(): number {
        return this._accuracy;
    }

    set accuracy(value: number) {
        this._accuracy = value;
    }

    get longitude(): number {
        return this._longitude;
    }

    set longitude(value: number) {
        this._longitude = value;
    }

    get latitude(): number {
        return this._latitude;
    }

    set latitude(value: number) {
        this._latitude = value;
    }

    get time(): number {
        return this._time;
    }

    set time(value: number) {
        this._time = value;
    }


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }


}
