import {AfterViewInit, ApplicationRef, ChangeDetectorRef, Component, Inject, NgZone, OnInit} from '@angular/core';
import {MarkerService} from '../../service/marker.service';
import {MyMarker} from '../../class/MyMarker';
import 'leaflet-routing-machine';
import {Router} from '@angular/router';
import {MapService} from '../../service/map.service';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements AfterViewInit {
    constructor(@Inject(MarkerService) private _markerService: MarkerService,
                @Inject(MapService) private _mapService: MapService,
                @Inject(Router) private router: Router,
                public alertController: AlertController) {
        if (this.markerService.list.length === 0) {
            this.router.navigateByUrl('/map');
            setTimeout(() => {
                this.router.navigateByUrl('/list');
            });
        }
    }

    ngAfterViewInit(): void {
        this.toURL("/map");
        setTimeout(() => this.toURL("/list"));
    }




    public async reallyDelete(): Promise<void> {
        const alert: HTMLIonAlertElement = await this.alertController.create({
            header: 'Löschen',
            message: 'Wollen sie wirklich alle ' + this.markerService.list.length + ' Einträge löschen?',
            buttons: [
                {
                    text: 'Nein',
                    role: 'cancel',
                    handler: () => null
                }, {
                    text: 'Ja',
                    handler: () => {
                        this.markerService.list.forEach((item) => {
                            this.remove(item);
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


    public toURL(url: string): void {
        if (url) {
            this.router.navigateByUrl(url);
        }
    }

    get markerService(): MarkerService {
        return this._markerService;
    }
    get mapService(): MapService {
        return this._mapService;
    }

    public remove(item: &MyMarker): void {
        item.marker.remove();
        this.markerService.deleteInList(item);
        this.mapService.mapGroup.removeLayer(item.marker);
    }


}
