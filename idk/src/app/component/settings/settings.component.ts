import {AfterViewInit, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {SettingsService} from '../../service/settings.service';
import {MapService} from '../../service/map.service';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements AfterViewInit, OnDestroy {


    constructor(@Inject(SettingsService) private _settingsService: SettingsService,
                @Inject(MapService) private _mapService: MapService,
                public alertController: AlertController) {

    }

    async ngAfterViewInit(): Promise<void> {
        return this.settingsService.getValueFromStorage();
    }

    ngOnDestroy(): void {
        this.settingsService.loadSettingsFromStorage = false;
        setTimeout(() => {
            this.settingsService.getValueFromStorage().then(r => null);
        });
    }

    public async saveBatteryOnBackgroundReally(eventAnswer: boolean = false): Promise<void> {
        if (eventAnswer) {
            const alert: HTMLIonAlertElement = await this.alertController.create({
                header: 'Energiesparmodus',
                message: 'Die Hintergrundaktualisierung werden dann Deaktiviert! Wollen sie wirklich den Energiesparmodus aktivieren?',
                buttons: [
                    {
                        text: 'Nein',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            this.settingsService.saveBatteryOnBackground = false;
                        }
                    }, {
                        text: 'Ja',
                        handler: () => {
                            this.settingsService.backgroundUpdate = false;
                            this.settingsService.saveBatteryOnBackground = true;
                        }
                    }
                ]
            });
            await alert.present();
        } else {
            this.settingsService.saveBatteryOnBackground = false;
        }

    }

    get settingsService(): SettingsService {
        return this._settingsService;
    }

    get mapService(): MapService {
        return this._mapService;
    }
}
