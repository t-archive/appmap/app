export const MY_MARKER_LAT: string = "lat";
export const MY_MARKER_LNG: string = "lng";
export const MY_MARKER_DESCRIPTION: string = "description";
export const MY_MARKER_SMALL_DESCRIPTION: string = "small_description";
export const MY_MARKER_ROUTING: string = "routing";
