// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const apiRoute: string = "http://192.168.1.192:5001/";
// tslint:disable-next-line:typedef
export const environment = {
  production: false,
  apiRoute: apiRoute,
  create: apiRoute + "mymarker/create",
  edit: apiRoute + "mymarker/edit",
  delete: apiRoute + "mymarker/delete",
  getAll: apiRoute + "mymarker/get",
  getById: apiRoute + "mymarker/get/id/",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
