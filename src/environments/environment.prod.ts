
const apiRoute: string = "http://192.168.1.192:5001/";
// tslint:disable-next-line:typedef
export const environment = {
  production: true,
  apiRoute: apiRoute,
  create: apiRoute + "mymarker/create",
  edit: apiRoute + "mymarker/edit",
  delete: apiRoute + "mymarker/delete",
  getAll: apiRoute + "mymarker/get",
  getById: apiRoute + "mymarker/get/id/",
};
