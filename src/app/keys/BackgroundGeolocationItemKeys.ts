export const BACKGROUND_GEOLOCATION_ITEM_ID: string = "id";
export const BACKGROUND_GEOLOCATION_ITEM_TIME: string = "time";
export const BACKGROUND_GEOLOCATION_ITEM_LATITUDE: string = "latitude";
export const BACKGROUND_GEOLOCATION_ITEM_LONGITUDE: string = "longitude";
export const BACKGROUND_GEOLOCATION_ITEM_ACCURACY: string = "accuracy";
export const BACKGROUND_GEOLOCATION_ITEM_SPEED: string = "speed";
export const BACKGROUND_GEOLOCATION_ITEM_ALTITUDE: string = "altitude";
export const BACKGROUND_GEOLOCATION_ITEM_BEARING: string = "bearing";
