export const MY_MARKER_ID: string = "id";
export const MY_MARKER_LAT: string = "latitude";
export const MY_MARKER_LNG: string = "longitude";
export const MY_MARKER_DESCRIPTION: string = "description";
export const MY_MARKER_SMALL_DESCRIPTION: string = "smallDescription";
export const MY_MARKER_ROUTING: string = "routing";
