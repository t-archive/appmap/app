import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {LeafletEvent, LeafletMouseEvent} from 'leaflet';
import {MarkerService} from '../../service/marker.service';
import {MyMarker} from '../../class/MyMarker';
import {GeoSearchControl, OpenStreetMapProvider} from 'leaflet-geosearch';
import 'leaflet.markercluster';
import 'leaflet-routing-machine';
import {
    BackgroundGeolocation
} from '@ionic-native/background-geolocation/ngx';
import {SettingsService} from '../../service/settings.service';
import {ActionSheetController, AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import {MapService} from '../../service/map.service';


@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {


    private _actionSheetOpen: boolean = false;

    constructor(@Inject(MapService) private _mapService: MapService,
                @Inject(SettingsService) private _settingsService: SettingsService,
                @Inject(MarkerService) private _markerService: MarkerService,
                public alertController: AlertController,
                public actionSheetController: ActionSheetController,
                private router: Router) {
        this.router.events.subscribe(x => {
            setTimeout(() => this.mapService.resizeMap());
        });
    }

    ngOnInit(): void {

    }

    async ngAfterViewInit(): Promise<void> {
        return this.mapService.start();
    }


    public log(log: any): void {
        console.log(log);
    }


    public async trackingIsNotActive(): Promise<void> {
        const alert: HTMLIonAlertElement = await this.alertController.create({
            header: 'Fehler',
            message: 'Die Ortungs-Funktion ist Deaktiviert! Wollen sie diese Aktivieren?',
            buttons: [
                {
                    text: 'Nein',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        this.mapService.markerService.liveTrackerSubject.next(false);
                    }
                }, {
                    text: 'Ja',
                    handler: () => {
                        this.settingsService.tracking = true;
                        this.mapService.markerService.liveTrackerSubject.next(true);
                        console.log('Confirm Okay');
                    }
                }
            ]
        });
        await alert.present();
    }


    public async presentActionSheet(): Promise<void> {
        if (!this._actionSheetOpen)  {
            const actionSheet: HTMLIonActionSheetElement = await this.actionSheetController.create({
                header: 'Linien Aufzeichnungen',
                buttons: [{
                    text: 'Starten',
                    handler: () => {
                        this.settingsService.recordedPolyline = true;
                    }
                }, {
                    text: 'Stoppen',
                    handler: () => {
                        this.settingsService.recordedPolyline = false;
                    }
                }, {
                    text: 'Löschen',
                    role: 'destructive',
                    handler: () => {

                        this.mapService.polylineService.deleteLine();
                        console.log('Delete clicked');
                    }
                }, {
                    text: 'Schließen',
                    role: 'cancel',
                    handler: () => {
                        this.actionSheetOpen = false;
                        console.log('Cancel clicked');
                    }
                }]
            });
            await actionSheet.present();
        }
    }


    get mapService(): MapService {
        return this._mapService;
    }

    get markerService(): MarkerService {
        return this._markerService;
    }

    get settingsService(): SettingsService {
        return this._settingsService;
    }

    get actionSheetOpen(): boolean {
        return this._actionSheetOpen;
    }

    set actionSheetOpen(value: boolean) {
        this._actionSheetOpen = value;
    }
}
