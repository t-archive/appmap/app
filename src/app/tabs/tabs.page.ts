import {Component, Inject} from '@angular/core';
import {MapService} from '../service/map.service';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage {
    constructor(@Inject(MapService) private _mapService: MapService) {
    }

    get mapService(): MapService {
        return this._mapService;
    }


}
