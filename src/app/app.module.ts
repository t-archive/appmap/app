import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ListComponent} from './component/list/list.component';
import {MapComponent} from './component/map/map.component';

import {IonicStorageModule} from '@ionic/storage';
import {SettingsComponent} from './component/settings/settings.component';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';
import {LongPressModule} from 'ionic-long-press';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [AppComponent, ListComponent, MapComponent, SettingsComponent],
    entryComponents: [],
    imports: [BrowserModule, CommonModule, IonicModule.forRoot({}),
        AppRoutingModule, FormsModule,
        LongPressModule,
        HttpClientModule,
        IonicStorageModule.forRoot({
            name: 'db',
            driverOrder: ['indexeddb', 'websql']
        })],
    providers: [
        StatusBar,
        SplashScreen,
        BackgroundGeolocation,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
