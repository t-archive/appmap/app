import {Inject, Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import * as L from 'leaflet';
import {MyMarker} from '../class/MyMarker';
import 'leaflet.markercluster';
import 'leaflet-routing-machine';
import {MarkerIcons} from '../keys/MarkerIcons';
import {Storage} from '@ionic/storage';
import {MARKER_LIST} from '../keys/StorageKeys';
import {MapService} from './map.service';
import {HttpService} from './http.service';


@Injectable({
    providedIn: 'root'
})
export class MarkerService {


    private _list: Array<MyMarker> = [];
    private _setRoutingMarker: Subject<MyMarker> = new Subject<MyMarker>();
    private _setMarker: Subject<MyMarker> = new Subject<MyMarker>();
    private _goToMarker: Subject<MyMarker> = new Subject<MyMarker>();
    private _reload: Subject<any> = new Subject<any>();
    private _enabledPopup: boolean = false;
    private _blockSetMarker: boolean = false;
    private _blockSetMarkerTemp: boolean = false;
    private _markerAreLoaded: boolean = false;
    private _liveMarker: L.Marker = null;
    private _liveTrackerSubject: Subject<boolean> = new Subject<boolean>();
    private _liveTracker: boolean = false;

    private _mapService: MapService = null;

    constructor(private storage: Storage, @Inject(HttpService) private httpService: HttpService) {
        this.liveTrackerSubject.subscribe(bool => {
            this.liveTracker = bool;
        });

    }

    public async loadMarker(): Promise<void> {
        if (!this.markerAreLoaded) {
            this.markerAreLoaded = true;
            return new Promise<any>((resolve, reject) => {
                this.httpService.getAll().subscribe((x) => {
                    const example: Array<MyMarker> = MyMarker.factoryArray(x.body);
                    example.forEach(item => {
                        item.marker = L.marker([item.lat, item.lng], MarkerIcons.markerIcon());
                        this.addMarker(item, true);
                    });
                    setTimeout(() => this.enabledPopup = true, 1000);
                    resolve();
                });
            });
            //
            // return this.storage.get(MARKER_LIST).then((x: any) => {
            //     if (x) {
            //         const example: Array<MyMarker> = MyMarker.factoryArray(JSON.parse(x));
            //         example.forEach(item => {
            //             item.marker = L.marker([item.lat, item.lng], MarkerIcons.markerIcon());
            //             this.addMarker(item, true);
            //         });
            //         setTimeout(() => this.enabledPopup = true, 1000);
            //     }
            // });
        } else {
            // this.list.forEach(x => x.marker.remove());
            this.list = [];
            this.markerAreLoaded = false;
            return this.loadMarker();
        }
    }


    public log(log: any): void {
        console.log(log);
    }

    public deleteInList(item: MyMarker): void {
        this.list = this.list.filter(x => x !== item);
        this.httpService.delete(item).subscribe((response) => {
            console.log(response);
        });
        this.removeRouterMarker(item);

    }

    public addMarker(marker: MyMarker, init: boolean = false): void {
        if (init || (this.blockSetMarker === false && this.blockSetMarkerTemp === false)) {
            this._list.push(marker);
            this.setMarker.next(marker);

        }
        if (!init) {
            this.httpService.create(marker).subscribe((response) => {
                console.log(response);
            });
        }
    }

    public updateHttp(myMarker: MyMarker): Promise<void> {
        return new Promise<void>((resolve => {
            this.httpService.edit(myMarker).subscribe((response) => {
                resolve();
            });
        }));
    }

    public addRoutingMarker(marker: &MyMarker): void {
        marker.routing = true;
        marker.marker.options.opacity = 1;

        this.setRoutingMarker.next(marker);
    }

    public findRouterMarker(item: MyMarker): MyMarker {
        return this.list.find(x => x === item && x.routing);
    }

    public removeRouterMarker(item: &MyMarker): void {
        item.routing = false;
        item.marker.options.opacity = 1;

        this.setRoutingMarker.next(new MyMarker());
        let counter: number = 0;
        this.list.forEach(x => {
            if (x.routing) {
                counter++;
            }
        });
        if (counter <= 1) {
            this.list.forEach((x: &MyMarker) => {
                if (x.routing) {
                    this.removeRouterMarker(x);
                }
            });
        }

    }


    public syncList(): void {
        this.storage.set(MARKER_LIST, JSON.stringify(this.list));
    }


    public findLocation(item: &MyMarker): void {
        if (!item.description && !item.smallDescription) {
            fetch(`https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&langCode=DE&location=${item.lng},${item.lat}`)
                .then(res => res.json())
                .then(myJson => {
                    if (myJson && myJson.address && myJson.address.LongLabel) {
                        if (!item.description) {
                            item.description = myJson.address.LongLabel;
                        }
                        if (!item.smallDescription || item.smallDescription === item.description) {
                            item.smallDescription = myJson.address.ShortLabel;
                        }
                        this.syncList();
                        this.reload.next(item.description);
                        // item.description = myJson.address.Match_addr;
                    }
                });
        }
    }

    public setLiveMarker(latLng: L.LatLng = null): void {
        if (!this.liveMarker) {
            if (latLng) {
                this.liveMarker = L.marker([latLng.lat, latLng.lng], MarkerIcons.trackerIcon());
            } else {
                this.liveMarker = L.marker([parseFloat(this.mapService.map.getCenter().lat.toFixed(3)), parseFloat(this.mapService.map.getCenter().lng.toFixed(3))], MarkerIcons.trackerIcon());
            }
            this.liveMarker.setOpacity(0);
            this.liveMarker.addTo(this.mapService.map);
        }
    }


    get list(): Array<MyMarker> {
        return this._list;
    }

    set list(value: Array<MyMarker>) {
        this._list = value;
        this.syncList();
    }

    get goToMarker(): Subject<MyMarker> {
        return this._goToMarker;
    }

    get setMarker(): Subject<MyMarker> {
        return this._setMarker;
    }

    get setRoutingMarker(): Subject<MyMarker> {
        return this._setRoutingMarker;
    }


    get reload(): Subject<any> {
        return this._reload;
    }

    get enabledPopup(): boolean {
        return this._enabledPopup;
    }

    set enabledPopup(value: boolean) {
        this._enabledPopup = value;
    }

    get blockSetMarker(): boolean {
        return this._blockSetMarker;
    }

    set blockSetMarker(value: boolean) {
        this._blockSetMarker = value;
    }

    get blockSetMarkerTemp(): boolean {
        return this._blockSetMarkerTemp;
    }

    set blockSetMarkerTemp(value: boolean) {
        this._blockSetMarkerTemp = value;
    }

    get mapService(): MapService {
        return this._mapService;
    }

    set mapService(value: MapService) {
        this._mapService = value;
    }


    get markerAreLoaded(): boolean {
        return this._markerAreLoaded;
    }

    set markerAreLoaded(value: boolean) {
        this._markerAreLoaded = value;
    }

    get liveTracker(): boolean {
        return this._liveTracker;
    }

    set liveTracker(value: boolean) {
        this._liveTracker = value;
    }

    get liveTrackerSubject(): Subject<boolean> {
        return this._liveTrackerSubject;
    }

    set liveTrackerSubject(value: Subject<boolean>) {
        this._liveTrackerSubject = value;
    }


    get liveMarker(): L.Marker {
        return this._liveMarker;
    }

    set liveMarker(value: L.Marker) {
        this._liveMarker = value;
    }
}
