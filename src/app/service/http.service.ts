import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MyMarker} from '../class/MyMarker';
import {environment} from '../../environments/environment';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HttpService {
    private options: any = {observe: "response", headers: "Content-Type: application/json"};
    constructor(@Inject(HttpClient) private http: HttpClient) {
    }

    public create(myMarker: MyMarker): Observable<any> {
        return this.http.post(environment.create, JSON.stringify(myMarker), this.options);
    }

    public edit(myMarker: MyMarker): Observable<any> {

        return this.http.put(environment.edit, JSON.stringify(myMarker), this.options);
    }

    public delete(myMarker: MyMarker): Observable<any> {
        return this.http.post(environment.delete, JSON.stringify(myMarker), this.options);
    }

    public getAll(): Observable<HttpResponse<MyMarker[]>> {
        return this.http.get<MyMarker[]>(environment.getAll, {
            observe: "response",
        });
    // .pipe(tap(x => {
    //         console.log(x);
    //     }))
    }

    public getById(id: number): Observable<MyMarker[]> {
        return this.http.get<MyMarker[]>(environment.getById + id);
    }
}
