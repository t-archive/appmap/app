import {Inject, Injectable} from '@angular/core';
import {SettingsService} from './settings.service';
import {
    BackgroundGeolocation,
    BackgroundGeolocationConfig,
    BackgroundGeolocationEvents, BackgroundGeolocationLocationProvider,
    BackgroundGeolocationResponse
} from '@ionic-native/background-geolocation/ngx';
import {MarkerService} from './marker.service';
import {MyMarker} from '../class/MyMarker';
import {MarkerIcons} from '../keys/MarkerIcons';
import {MapService} from './map.service';
import {PolylineService} from './polyline.service';
import {Storage} from '@ionic/storage';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LocationService {

    private _mapService: MapService = null;
    private _locationList: BackgroundGeolocationResponse[] = [];
    private _polylineService: PolylineService = null;
    private _location: Subject<BackgroundGeolocationResponse> = new Subject<BackgroundGeolocationResponse>();
    private _isLoaded: boolean = false;

    constructor(@Inject(SettingsService) private _settingsService: SettingsService,
                private _backgroundGeolocation: BackgroundGeolocation,
                private storage: Storage,
                @Inject(MarkerService) private _markerService: MarkerService) {
    }

    public start(): Promise<void> {
        return this.polylineService.loadLinesFromStorage().then(() => {
            return this.loadBackgroundGeolocation().then(() => {
                return this.polylineService.syncLocationStorage().then(() => {
                    return this.initMapGeoSubject().then(() => {
                        this.mapService.resizeMap();
                        return this.configureBackgroundGeolocation().then(() => {
                            this.isLoaded = true;
                            this.mapService.resizeMap();
                        });
                    });
                });
            });
        });
    }


    private async loadBackgroundGeolocation(): Promise<void> {
        return this._backgroundGeolocation.getLocations().then((locationArray: BackgroundGeolocationResponse[]) => {
            locationArray = locationArray.sort((x, y) => {
                return x.time - y.time;
            });
            // this.polylineService.createLine(null, {color: 'blue'}, locationArray);
            return locationArray.forEach(async (item) => {
                this.polylineService.createLine(item, {color: 'black'}, null, 2); // TODO: BLUE
                await this.backgroundGeolocation.deleteLocation(item.id);
            });
            // return this.backgroundGeolocation.deleteAllLocations();
        });
    }


    private async configureBackgroundGeolocation(): Promise<any> {
        return this._backgroundGeolocation.configure(this.getMapGeoConfig()).then(() => {
            if (!this.isLoaded) {
                return this.initMapGeo();
            }
        });
    }

    private async initMapGeo(): Promise<any> {
        if (this.settingsService.tracking && (this.settingsService.liveTracking || this.markerService.liveTracker)) {
            await this._backgroundGeolocation.on(BackgroundGeolocationEvents.location)
                .subscribe(async (location: BackgroundGeolocationResponse) => {
                    if (this.settingsService.tracking) {
                        this._location.next(location);
                    }
                    if (this.settingsService.tracking && (this.settingsService.liveTracking || this.markerService.liveTracker)) {
                        // console.log((location.speed * 3.688).toFixed(0) + ' km/h');
                        // console.log((location.speed * 2.237).toFixed(0) + ' mp/h');
                        const item: MyMarker = new MyMarker(location.latitude, location.longitude, MarkerIcons.trackerIcon());
                        if (this.markerService.liveTracker) {
                            setTimeout(() => this.markerService.goToMarker.next(item), 500);
                        }
                        this.markerService.liveMarker.setOpacity(1);
                        this.markerService.liveMarker.setLatLng(item.marker.getLatLng());

                        this._polylineService.createLine(location, {color: 'black'}, null, 0, this.settingsService.recordedPolyline);
                        await this._backgroundGeolocation.finish(); // FOR IOS ONLY
                        await Promise.all([
                            this._backgroundGeolocation.deleteLocation(location.id),
                            this.polylineService.syncLocationStorage()
                        ]);
                    }
                });
            return this._backgroundGeolocation.start();

        }
    }

    private getMapGeoConfig(): BackgroundGeolocationConfig {
        return {
            locationProvider: BackgroundGeolocationLocationProvider.ACTIVITY_PROVIDER,
            desiredAccuracy: 0,
            stationaryRadius: 3,
            distanceFilter: 3,
            interval: 2000,
            fastestInterval: (this.settingsService.saveBatteryOnBackground ? 2000 : 250),
            activitiesInterval: 2000,
            stopOnStillActivity: true,
            startForeground: true,
            debug: false,
            maxLocations: 50000,
            stopOnTerminate: !this.settingsService.backgroundUpdate, // all
            startOnBoot: this.settingsService.backgroundUpdate ? true : (false), // android
            pauseLocationUpdates: !this.settingsService.backgroundUpdate, // ios (Stoppt die Ortung wenn App im Hintergrund läuft)
            saveBatteryOnBackground: this.settingsService.saveBatteryOnBackground, // ios
            notificationTitle: 'Hintergrund-Ortung',
            notificationText: 'Diese Ortet sie im Hintergrund weiter!'
        };
    }


    private async initMapGeoSubject(): Promise<void> {
        if (this.settingsService.liveTracking) {
            this.markerService.setLiveMarker();
        }
        this.settingsService.trackingSubject.subscribe(async (bool) => {
            if (bool === false) {
                await this.cancelLive();
            }
        });

        this.settingsService.liveTrackingSubject.subscribe(async (bool) => {
            if (this.settingsService.tracking) {
                if (bool === true) {
                    this.markerService.setLiveMarker();
                    if (this.isLoaded) {
                        await this.initMapGeo();
                    }
                } else {
                    await this.cancelLive();
                }
            }
        });

        this.markerService.liveTrackerSubject.subscribe(async (bool) => {
            if (bool === true) {
                this.markerService.setLiveMarker();
                this.settingsService.saveBatteryOnBackground = false;
                if (this.isLoaded) {
                    await this.initMapGeo();
                }
            } else {
                if (!this.settingsService.liveTracking) {
                    await this.cancelLive();
                }
            }
        });
    }

    private async cancelLive(): Promise<void> {
        if (this.markerService.liveMarker) {
            this.markerService.liveMarker.setOpacity(0);
        }
        await this.backgroundGeolocation.stop();
        if (this.isLoaded) {
            return this.start();
        }
    }

    get markerService(): MarkerService {
        return this._markerService;
    }


    get settingsService(): SettingsService {
        return this._settingsService;
    }


    get mapService(): MapService {
        return this._mapService;
    }


    set mapService(value: MapService) {
        this._mapService = value;
    }

    get locationList(): BackgroundGeolocationResponse[] {
        return this._locationList;
    }

    set locationList(value: BackgroundGeolocationResponse[]) {
        this._locationList = value;
    }

    get polylineService(): PolylineService {
        return this._polylineService;
    }

    set polylineService(value: PolylineService) {
        this._polylineService = value;
    }

    get backgroundGeolocation(): BackgroundGeolocation {
        return this._backgroundGeolocation;
    }

    get isLoaded(): boolean {
        return this._isLoaded;
    }

    set isLoaded(value: boolean) {
        this._isLoaded = value;
    }
}
