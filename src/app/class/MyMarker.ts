import * as leaflet from 'leaflet';
import * as L from 'leaflet';
import {
    MY_MARKER_DESCRIPTION,
    MY_MARKER_ID,
    MY_MARKER_LAT,
    MY_MARKER_LNG,
    MY_MARKER_ROUTING,
    MY_MARKER_SMALL_DESCRIPTION
} from '../keys/MyMarkerKeys';
import {checkIfBoolExists, checkIfNumberExists, checkIfStringExists} from '../helper/FactoryHelper';

export class MyMarker {
    private _id?: number;
    private _lat?: number;
    private _lng?: number;
    private _description?: &string;
    private _smallDescription?: &string;
    private _marker?: &leaflet.Marker;
    private _waypoint: L.Routing.Waypoint = null;
    private _routing: boolean | number = false;


    public toJSON(): object {
        const item: object = {};
        item[MY_MARKER_ID] = this.id;
        item[MY_MARKER_LAT] = this.lat;
        item[MY_MARKER_LNG] = this.lng;
        item[MY_MARKER_DESCRIPTION] = this.description;
        item[MY_MARKER_SMALL_DESCRIPTION] = this.smallDescription;
        item[MY_MARKER_ROUTING] = this.routing;
        return item;
    }

    public static factory(raw: any): MyMarker {
        const item: MyMarker = new MyMarker();
        if (raw) {
            if (checkIfNumberExists(raw, MY_MARKER_ID) && raw[MY_MARKER_ID]) {
                item.id = raw[MY_MARKER_ID];
            }
            if (checkIfNumberExists(raw, MY_MARKER_LAT) && raw[MY_MARKER_LAT]) {
                item.lat = raw[MY_MARKER_LAT];
            }
            if (checkIfNumberExists(raw, MY_MARKER_LNG) && raw[MY_MARKER_LNG]) {
                item.lng = raw[MY_MARKER_LNG];
            }
            if (checkIfStringExists(raw, MY_MARKER_DESCRIPTION) && raw[MY_MARKER_DESCRIPTION]) {
                item.description = raw[MY_MARKER_DESCRIPTION];
            }
            if (checkIfStringExists(raw, MY_MARKER_SMALL_DESCRIPTION) && raw[MY_MARKER_SMALL_DESCRIPTION]) {
                item.smallDescription = raw[MY_MARKER_SMALL_DESCRIPTION];
            }
            if (checkIfNumberExists(raw, MY_MARKER_ROUTING) && raw[MY_MARKER_ROUTING]) {
                item.routing = raw[MY_MARKER_ROUTING];
            }
            if (checkIfBoolExists(raw, MY_MARKER_ROUTING) && raw[MY_MARKER_ROUTING]) {
                item.routing = raw[MY_MARKER_ROUTING];
            }
        }
        return item;
    }
    public static factoryArray(raw: any): MyMarker[] {
        const array: MyMarker[] = [];
        for (const item of raw) {
            array.push(this.factory(item));
        }
        return array;
    }

    constructor(lat?: number, lng?: number, icon?: { icon: L.Icon<L.IconOptions> }) {
        if (lat) {
            this.lat = lat;
        }
        if (lng) {
            this.lng = lng;
        }
        if (icon) {
            this.marker = L.marker([this.lat, this.lng], icon);
        }
    }

    public generateWaypoint(): void {
        this.waypoint = new L.Routing.Waypoint(this.marker.getLatLng(), this.description, {
            allowUTurn: false
        });
    }

    get marker(): &leaflet.Marker {
        return this._marker;
    }

    set marker(value: &leaflet.Marker) {
        this._marker = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get smallDescription(): string {
        if (!this._smallDescription) {
            return this._description;
        } else {
            return this._smallDescription;
        }

    }

    set smallDescription(value: string) {
        this._smallDescription = value;
    }


    get waypoint(): L.Routing.Waypoint {
        return this._waypoint;
    }

    set waypoint(value: L.Routing.Waypoint) {
        this._waypoint = value;
    }

    get lng(): number {
        return this._lng;
    }

    set lng(value: number) {
        this._lng = value;
    }

    get lat(): number {
        return this._lat;
    }

    set lat(value: number) {
        this._lat = value;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get routing(): boolean | number {
        return this._routing;
    }

    set routing(value: boolean | number) {
        this._routing = value;
    }
}
